﻿using System;
using System.Collections.Generic;

namespace network
{
    public class Network
    {
        public Network(IList<byte[]> trainingSet, IList<byte> answers)
        {
            if (trainingSet == null) throw new ArgumentNullException(nameof(trainingSet));
            if (answers == null) throw new ArgumentNullException(nameof(answers));

            var inputLayer = new InputLayer<byte>(trainingSet, answers);
        }
    }
}