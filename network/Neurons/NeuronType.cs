﻿namespace network.Neurons
{
    public enum NeuronType
    {
        Hidden,
        Output
    }
}