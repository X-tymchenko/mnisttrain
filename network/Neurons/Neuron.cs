﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using network.Activations;

namespace network.Neurons
{
    public class Neuron
    {
        private readonly Activation _activation;


        public Neuron(double[] inputs, double[] weights, NeuronType type, ActivationType aType)
        {
            if (!Enum.IsDefined(typeof(NeuronType), type))
                throw new InvalidEnumArgumentException(nameof(type), (int) type, typeof(NeuronType));
            NeuronType = type;
            Weights = weights;
            Inputs = inputs;

            _activation = new Activation(aType);
        }

        private NeuronType NeuronType { get; }

        public double[] Weights { get; set; }

        public double[] Inputs { get; set; }

        public double Output => Activator(Inputs, Weights);

        private double Activator(IReadOnlyList<double> i, IReadOnlyList<double> w)
        {
            if (w.Count == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(w));
            if (i.Count == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(i));
            var sum = i.Select((t, l) => t * w[l]).Sum();
            return _activation.AFunction(sum);
        }
        
        /// <summary>
        /// Формула похідної для поточної функції активації
        /// </summary>
        /// <param name="outsignal"></param>
        /// <returns></returns>
        public double Derivativator(double outsignal) =>
            _activation.ActivationDerivative(outsignal);

        public double Gradientor(double error, double dif, double gSum) =>
            (NeuronType == NeuronType.Output) ? error * dif : gSum * dif; //g_sum - это сумма градиентов следующего слоя
    }
}