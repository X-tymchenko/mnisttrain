﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace network
{
    public class MnistImage
    {
        private readonly int _width; // 28
        private readonly int _height; // 28

        public readonly IList<byte> Pixels;

        public byte Label { get; }

        private MnistImage(int width, int height, byte[,] pixels, byte label)
        {
            _width = width;
            _height = height;
            Pixels = new List<byte>(_width * _height);

            for (var i = 0; i < _height; i++)
            for (var j = 0; j < _width; j++)
                Pixels.Add(pixels[i, j]);

            Label = label;
        }

        public static MnistImage[] LoadData(string trainMnistTrainImagesIdx3Ubyte,
            string trainMnistTrainLabelsIdx1Ubyte)
        {
            var sfImageFileStream = new FileStream(trainMnistTrainImagesIdx3Ubyte, FileMode.Open, FileAccess.Read);
            var sfLabelFileStream = new FileStream(trainMnistTrainLabelsIdx1Ubyte, FileMode.Open, FileAccess.Read);

            var brImages = new BinaryReader(sfImageFileStream);
            var brLabels = new BinaryReader(sfLabelFileStream);

            var magic1 = ReverseBytes(brImages.ReadInt32()); //целочисленное значение (32 бита), равное 2051
            var numImages =
                ReverseBytes(brImages
                    .ReadInt32()); // количество изображений (целочисленное значение) 60000, преобразуем в формат Intel
            var numRow = (int) ReverseBytes(brImages.ReadInt32()); // rows 28
            var numCol = (int) ReverseBytes(brImages.ReadInt32()); // cols 28
            var magic2 = ReverseBytes(brLabels.ReadInt32()); // magic number
            var numLabels = ReverseBytes(brLabels.ReadInt32()); // count labels

            var result = new MnistImage[numImages];

            var pixels = new byte[numRow, numCol];
            for (var di = 0; di < numImages; ++di)
            {
                for (var i = 0; i < numRow; ++i) // получаем пиксельные значения 28x28
                for (var j = 0; j < numCol; ++j)
                    pixels[i, j] = brImages.ReadByte();
                var lbl = brLabels.ReadByte(); // получаем маркеры
                var dImage = new MnistImage(numRow, numCol, pixels, lbl);

                result[di] = dImage;
            } // по каждому изображению

            sfImageFileStream.Close();
            brImages.Close();
            sfLabelFileStream.Close();
            brLabels.Close();
            return result;
        }

        // c форматa (big endian format) ( обратным порядком байтов), используемым некоторыми процессорами, отличными от Intel,
        // преобразуем в более привычный «остроконечном» формате (little endian format)
        private static uint ReverseBytes(int v)
        {
            var value = checked((uint) v);
            return (value & 0x0000_00FFU) << 24 | (value & 0x0000_FF00U) << 8 |
                   (value & 0x00FF_0000U) >> 8 | (value & 0xFF00_0000U) >> 24;
        }

        public override string ToString()
        {
            var s = new StringBuilder(_width * _height + _width * 2 + 1);
            for (var i = 0; i < Pixels.Count; i++)
            {
                if (i % 28 == 0)
                    s.Append("\n");
                s.Append(Pixels[i] == 0 ? "_" : "@");
            }
            return s.Append(Label).ToString();
        }
    }
}