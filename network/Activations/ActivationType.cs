﻿namespace network.Activations
{
    public enum ActivationType
    {
        Identity,
        BinaryStep,
        Logistic,
        Tanh,
        ArcTan,
        ReLu,
        PReLU,
        Elu,
        SoftPlus,
        BentIdentity,
        Sinusoid,
        Sinc,
        Gaussian,
        Bipolar,
        BipolarSigmoid
    }
}