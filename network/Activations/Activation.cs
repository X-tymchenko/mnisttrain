﻿using System;

namespace network.Activations
{
    ///<summary>
    ///Activation Functions from:
    ///https://en.wikipedia.org/wiki/Activation_function
    ///https://stats.stackexchange.com/questions/115258/comprehensive-list-of-activation-functions-in-neural-networks-with-pros-cons
    ///D infront means the Derivative of the function
    ///x is the input of one perceptron. a is the alpha value sometimes needed.
    ///</summary>
    [Serializable]
    public class Activation
    {
        private readonly ActivationType _activationType;

        public Activation(ActivationType type)
        {
            _activationType = type;
        }

        public double AFunction(double x)
        {
            switch (_activationType)
            {
                case ActivationType.Identity:
                    return Identity(x);
                case ActivationType.BinaryStep:
                    return BinaryStep(x);
                case ActivationType.Logistic:
                    return Logistic(x);
                case ActivationType.Tanh:
                    return Tanh(x);
                case ActivationType.ArcTan:
                    return ArcTan(x);
                case ActivationType.ReLu:
                    return ReLu(x);
                case ActivationType.SoftPlus:
                    return SoftPlus(x);
                case ActivationType.BentIdentity:
                    return BentIdentity(x);
                case ActivationType.Sinusoid:
                    return Sinusoid(x);
                case ActivationType.Sinc:
                    return Sinc(x);
                case ActivationType.Gaussian:
                    return Gaussian(x);
                case ActivationType.Bipolar:
                    return Bipolar(x);
                case ActivationType.BipolarSigmoid:
                    return BipolarSigmoid(x);
                case ActivationType.PReLU:
                    break;
                case ActivationType.Elu:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return 0;
        }

        public double ActivationDerivative(double x)
        {
            switch (_activationType)
            {
                case ActivationType.Logistic:
                    return DLogistic(x);
                case ActivationType.Tanh:
                    return DTanh(x);
                case ActivationType.ArcTan:
                    return DArcTan(x);
                case ActivationType.ReLu:
                    return DReLU(x);
                case ActivationType.SoftPlus:
                    return DSoftPlus(x);
                case ActivationType.BentIdentity:
                    return DBentIdentity(x);
                case ActivationType.Sinusoid:
                    return DSinusoid(x);
                case ActivationType.Sinc:
                    return DSinc(x);
                case ActivationType.Gaussian:
                    return DGaussian(x);
                case ActivationType.BipolarSigmoid:
                    return DBipolarSigmoid(x);
                case ActivationType.Identity:
                    break;
                case ActivationType.BinaryStep:
                    break;
                case ActivationType.PReLU:
                    break;
                case ActivationType.Elu:
                    break;
                case ActivationType.Bipolar:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return 0;
        }

        public double AFunction(double x, double a)
        {
            switch (_activationType)
            {
                case ActivationType.PReLU:
                    return PReLU(x, a);
                case ActivationType.Elu:
                    return Elu(x, a);
                case ActivationType.Identity:
                    break;
                case ActivationType.BinaryStep:
                    break;
                case ActivationType.Logistic:
                    break;
                case ActivationType.Tanh:
                    break;
                case ActivationType.ArcTan:
                    break;
                case ActivationType.ReLu:
                    break;
                case ActivationType.SoftPlus:
                    break;
                case ActivationType.BentIdentity:
                    break;
                case ActivationType.Sinusoid:
                    break;
                case ActivationType.Sinc:
                    break;
                case ActivationType.Gaussian:
                    break;
                case ActivationType.Bipolar:
                    break;
                case ActivationType.BipolarSigmoid:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return 0;
        }

        public double ActivationDerivative(double x, double a)
        {
            switch (_activationType)
            {
                case ActivationType.PReLU:
                    return DPReLU(x, a);
                case ActivationType.Elu:
                    return Delu(x, a);
                case ActivationType.Identity:
                    break;
                case ActivationType.BinaryStep:
                    break;
                case ActivationType.Logistic:
                    break;
                case ActivationType.Tanh:
                    break;
                case ActivationType.ArcTan:
                    break;
                case ActivationType.ReLu:
                    break;
                case ActivationType.SoftPlus:
                    break;
                case ActivationType.BentIdentity:
                    break;
                case ActivationType.Sinusoid:
                    break;
                case ActivationType.Sinc:
                    break;
                case ActivationType.Gaussian:
                    break;
                case ActivationType.Bipolar:
                    break;
                case ActivationType.BipolarSigmoid:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return 0;
        }

        private double Identity(double x) => x;

        private double BinaryStep(double x) => x < 0 ? 0 : 1;

        private double Logistic(double x) => 1 / (1 + Math.Pow(Math.E, -x));

        private double DLogistic(double x) => Logistic(x) * (1 - Logistic(x));

        private double Tanh(double x) => 2 / (1 + Math.Pow(Math.E, -(2 * x))) - 1;

        private double DTanh(double x) => 1 - Math.Pow(Tanh(x), 2);

        private double ArcTan(double x) => Math.Atan(x);

        private double DArcTan(double x) => 1 / Math.Pow(x, 2) + 1;

        //Rectifnear Unit
        private double ReLu(double x) => Math.Max(0, x);

        private double DReLU(double x) => Math.Max(0, 1);

        //ParameRectified Linear Unit 
        private double PReLU(double x, double a) => x < 0 ? a * x : x;

        private double DPReLU(double x, double a) => x < 0 ? a : 1;

        //Exponential Linear Unit 
        private double Elu(double x, double a) => x < 0 ? a * (Math.Pow(Math.E, x) - 1) : x;

        private double Delu(double x, double a) => x < 0 ? Elu(x, a) + a : 1;

        private double SoftPlus(double x) => Math.Log(Math.Exp(x) + 1);

        private double DSoftPlus(double x) => Logistic(x);

        private double BentIdentity(double x) => (Math.Sqrt(Math.Pow(x, 2) + 1) - 1) / 2 + x;

        private double DBentIdentity(double x) => x / (2 * Math.Sqrt(Math.Pow(x, 2) + 1)) + 1;

        private double Sinusoid(double x) => Math.Sin(x);

        private double DSinusoid(double x) => Math.Cos(x);

        private double Sinc(double x) => x == 0 ? 1 : Math.Sin(x) / x;

        private double DSinc(double x) => x == 0 ? 0 : Math.Cos(x) / x - Math.Sin(x) / Math.Pow(x, 2);

        private double Gaussian(double x) => Math.Pow(Math.E, Math.Pow(-x, 2));

        private double DGaussian(double x) => -2 * x * Math.Pow(Math.E, Math.Pow(-x, 2));

        private double Bipolar(double x) => x < 0 ? -1 : 1;

        private double BipolarSigmoid(double x) => (1 - Math.Exp(-x)) / (1 + Math.Exp(-x));

        private double DBipolarSigmoid(double x) => 0.5 * (1 + BipolarSigmoid(x)) * (1 - BipolarSigmoid(x));

        private double Scaler(double x, double min, double max) => (x - min) / (max - min);
    }
}