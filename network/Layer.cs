﻿using System.Globalization;
using System.Xml;
using network.Neurons;
using network.Activations;
using static System.Console;

namespace network
{
    public abstract class Layer
    {
        protected Layer(int countNeuronsInCorrentLayer, int countNeuronsInPrevLayer, NeuronType nt, string type,
            ActivationType activationType)
        {
            //увидите это в WeightInitialize
            _countNeuronsInCorrentLayer = countNeuronsInCorrentLayer;
            _countNeuronsInPrevLayer = countNeuronsInPrevLayer;
            Neurons = new Neuron[countNeuronsInCorrentLayer];
            var weights = WeightInitialize(MemoryMode.Get, type);
            var tempWeights = new double[countNeuronsInPrevLayer];
            for (var i = 0; i < countNeuronsInCorrentLayer; ++i)
            {
                for (var j = 0; j < countNeuronsInPrevLayer; ++j)
                    tempWeights[j] = weights[i, j];
                Neurons[i] = new Neuron(null, tempWeights, nt, activationType); //про подачу null на входы ниже
            }
        }

        protected readonly int _countNeuronsInCorrentLayer;
        protected readonly int _countNeuronsInPrevLayer; 

        protected const double LearningRate = 0.1d;

        protected Neuron[] Neurons { get; }

        protected double[] Data //я подал null на входы нейронов, так как
        {
            //сначала нужно будет преобразовать информацию
            set //(видео, изображения, etc.)
            {
                //а загружать input'ы нейронов слоя надо не сразу,
                foreach (var t in Neurons)
                    t.Inputs = value;
            } //а только после вычисления выходов предыдущего слоя
        }

        protected double[,] WeightInitialize(MemoryMode mm, string type)
        {
            var weights = new double[_countNeuronsInCorrentLayer, _countNeuronsInPrevLayer];
            WriteLine($"{type} weights are being initialized...");
            var memoryDoc = new XmlDocument();
            memoryDoc.Load($"{type}_memory.xml");
            var memoryEl = memoryDoc.DocumentElement;
            switch (mm)
            {
                case MemoryMode.Get:
                    for (var l = 0; l < weights.GetLength(0); ++l)
                    for (var k = 0; k < weights.GetLength(1); ++k)
                        weights[l, k] =
                            double.Parse(
                                memoryEl.ChildNodes.Item(k + weights.GetLength(1) * l).InnerText.Replace(',', '.'),
                                CultureInfo.InvariantCulture); //parsing stuff
                    break;
                case MemoryMode.Set:
                    for (var l = 0; l < Neurons.Length; ++l)
                    for (var k = 0; k < _countNeuronsInPrevLayer; ++k)
                        memoryEl.ChildNodes.Item(k + _countNeuronsInPrevLayer * l).InnerText =
                            Neurons[l].Weights[k].ToString(CultureInfo.InvariantCulture);
                    break;
            }

            memoryDoc.Save($"{type}_memory.xml");
            WriteLine($"{type} weights have been initialized...");
            return weights;
        }

        public abstract void Recognize(Network net, Layer nextLayer);
        public abstract double[] BackwardPass(double[] stuff);
    }
}