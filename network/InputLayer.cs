﻿using System;
using System.Collections.Generic;

namespace network
{
    /// <summary>
    /// Клас який приймає шаблон. Відповідає за навчальну вибірку та відповіді для нейронної мережі
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class InputLayer<T>
    {
        /// <summary>
        /// Поле класу яке зберігає навчальну вибірку.Є списком який містить навчальну вибірку. Поле є типом readonly.
        /// </summary>
        public readonly IList<T[]> TrainingSet;

        /// <summary>
        /// Поле класу яке зберігає навчальну вибірку. Є списком який містить відповіді. Поле є типом readonly
        /// </summary>
        public readonly IList<T> Answers;

        /// <summary>
        /// Конструктор початкового шару.
        /// </summary>
        /// <param name="trainingSet">Навчальна вибірка</param>
        /// <param name="answers">Відповіді для нейронної мережі</param>
        public InputLayer(IList<T[]> trainingSet, IList<T> answers)
        {
            if (trainingSet.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(trainingSet));
            if (answers.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(answers));
            TrainingSet = trainingSet;
            Answers = answers;
        }
    }
}