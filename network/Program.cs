﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static System.Char;
using static System.Console;

namespace network
{
    internal static class Program
    {
        private const string PixelFile = @"TrainMnist/train-images.idx3-ubyte";
        private const string LabelFile = @"TrainMnist/train-labels.idx1-ubyte";

        private const string TestPixelFile = @"TrainMnist/t10k-images.idx3-ubyte";
        private const string TestLabelFile = @"TrainMnist/t10k-labels.idx1-ubyte";

        private static MnistImage[] _mnistImages;
        private static MnistImage[] _trainImages;

        private static readonly IList<byte[]> InputsCollection = new List<byte[]>();
        private static readonly IList<byte> Answers = new List<byte>();

        private static void Main(string[] args)
        {
            _mnistImages = MnistImage.LoadData(PixelFile, LabelFile);
            _trainImages = MnistImage.LoadData(TestPixelFile, TestLabelFile);


            foreach (var image in _mnistImages)
            {
                InputsCollection.Add(image.Pixels.ToArray());
                Answers.Add(image.Label);
            }

            var net = new Network(InputsCollection, Answers);

        }
    }
}